package grsu.controllers;

public class ModelTable {
    String index;
    String street;
    Integer id;
    Integer adIndex;

    public ModelTable(String index, String street, Integer id) {
        this.index = index;
        this.street = street;
        this.id = id;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
