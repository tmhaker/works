package grsu.controllers;

import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.omg.PortableInterceptor.INACTIVE;

public class AddController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField addStreet;

    @FXML
    private TextField addIndex;

    @FXML
    private Button addBtn;

    @FXML
    private Button cancel;


    @FXML
    void initialize() {

    }

    public void actionClose(ActionEvent actionEvent) {
        Node source = (Node) actionEvent.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

    public void actionSave(ActionEvent actionEvent) {


        if ((addIndex.getText().trim().isEmpty()) || (addStreet.getText().trim().isEmpty())) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("ВНИМАНИЕ!");
            alert.setHeaderText(null);
            alert.setContentText("Поля не должны быть пустыми!!! Нечего добавить в БД!!!");
            alert.showAndWait();
        } else {
            Connection connection = null;
            try {
                connection = DBConnector.getConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            Integer index = Integer.parseInt(addIndex.getText());
            String street = addStreet.getText();
            Integer id;
            Statement statement = null;
            try {
                statement = connection.createStatement();
                statement.execute("INSERT INTO hrodno_index.table_ind(`index`) VALUES(" + index + ");");

                ResultSet rs=statement.executeQuery("SELECT * FROM hrodno_index.table_ind WHERE `index`="+index+";");
                rs.next();
                id=rs.getInt("id");
                statement.execute("INSERT INTO hrodno_index.table_st(`street`,`table_ind_id`) VALUES('" + street + "',"+id+");");
                
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                //finally block used to close resources
                try {
                    if (statement != null)
                        connection.close();
                } catch (SQLException se) {
                }// do nothing
                try {
                    if (connection != null)
                        connection.close();
                } catch (SQLException se) {
                    se.printStackTrace();
                }//end finally try
            }//end try
            actionClose(actionEvent);
        }
    }
}