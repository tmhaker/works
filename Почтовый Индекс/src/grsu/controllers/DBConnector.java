package grsu.controllers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBConnector {
    public static Connection getConnection() throws SQLException {
        Connection connection =
                DriverManager.getConnection
                        ("jdbc:mysql://localhost:3306/hrodno_index?autoReconnect=true&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC",
                                "root", "");
        return connection;

    }
}

