package grsu.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class TableController implements Initializable {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField street;

    @FXML
    private TableView<ModelTable> tableD;

    @FXML
    private TableColumn<ModelTable, String> columnIndex;

    @FXML
    private TableColumn<ModelTable, String> columnStreet;

    @FXML
    private Button loadBtn;


    @FXML
    private Button deleteBtn;

    @FXML
    void addMenu(ActionEvent event) {
    try {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("../start/addMenu.fxml"));
        stage.setTitle("Окно добавления");
        stage.setResizable(false);
        stage.setScene(new Scene(root, 550, 100));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(((Node)event.getSource()).getScene().getWindow());
        stage.show();
    }catch (IOException e){
        e.printStackTrace();
    }
    }

    @FXML
    void deleteAction(ActionEvent event) {
        ModelTable md = tableD.getSelectionModel().getSelectedItem();
        String query = "DELETE FROM hrodno_index.table_st WHERE id = ?;";
        try(PreparedStatement sta = DBConnector.getConnection().prepareStatement(query)) {
            sta.setInt(1, md.getId());
            sta.executeUpdate();

        }catch (SQLException ex) {
            Logger.getLogger(TableController.class.getName()).log(Level.SEVERE,null,ex);
        }
        oblist.remove(md);

    }

    ObservableList<ModelTable> oblist= FXCollections.observableArrayList();



    @SuppressWarnings("Duplicates")
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try(Connection con = DBConnector.getConnection()) {

        ResultSet rs=con.createStatement().executeQuery("SELECT st.street, st.id" +
                ", ind.index FROM hrodno_index.table_st st, hrodno_index.table_ind ind" +
                " WHERE st.table_ind_id=ind.id");

        while (rs.next()){
            oblist.add(new ModelTable(rs.getString("index"), rs.getString("street"), rs.getInt("id")));
        }

        } catch (SQLException ex) {
            Logger.getLogger(TableController.class.getName()).log(Level.SEVERE, null, ex);
        }

        columnIndex.setCellValueFactory(new PropertyValueFactory<>("index"));
        columnStreet.setCellValueFactory(new PropertyValueFactory<>("street"));

        tableD.setItems(oblist);

        FilteredList<ModelTable> filteredData =new FilteredList(oblist, e ->true);

        street.textProperty().addListener((observable, oldValue, newValue)->{
            filteredData.setPredicate(ModelTable->{
                if(newValue==null || newValue.isEmpty()){
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (ModelTable.getIndex().toLowerCase().contains(lowerCaseFilter)){
                    return true;
                } else if (ModelTable.getStreet().toLowerCase().contains(lowerCaseFilter)){
                    return true;
                }
                return false;
            });
        });

        SortedList<ModelTable> sortedData = new SortedList<>(filteredData);

        sortedData.comparatorProperty().bind(tableD.comparatorProperty());
        tableD.setItems(sortedData);


    }

    @SuppressWarnings("Duplicates")
    public void updateAction(ActionEvent actionEvent) {
        oblist.clear();
        try (Connection con = DBConnector.getConnection()) {

            ResultSet rs = con.createStatement().executeQuery("SELECT st.street, st.id" +
                    ", ind.index FROM hrodno_index.table_st st, hrodno_index.table_ind ind" +
                    " WHERE st.table_ind_id=ind.id");

            while (rs.next()) {
                oblist.add(new ModelTable(rs.getString("index"), rs.getString("street"), rs.getInt("id")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(TableController.class.getName()).log(Level.SEVERE, null, ex);
        }

        columnIndex.setCellValueFactory(new PropertyValueFactory<>("index"));
        columnStreet.setCellValueFactory(new PropertyValueFactory<>("street"));

        tableD.setItems(oblist);
    }
}
