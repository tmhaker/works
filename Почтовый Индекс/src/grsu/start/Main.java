package grsu.start;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

        @Override
        public void start(Stage primaryStage) throws Exception{
            Parent root = FXMLLoader.load(getClass().getResource("main.fxml"));
            primaryStage.setTitle("Почтовые Индексы Гродно");
            primaryStage.setMinHeight(445);
            primaryStage.setMinWidth(540);
            primaryStage.setResizable(false);
            primaryStage.setScene(new Scene(root, 540, 445));
            primaryStage.show();
        }


    public static void main(String[] args) {launch(args);
    }
}
