package witaless.simplecart;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<Product> mOrder = new ArrayList<>();
    private double[] mPrice = new double[3];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void onProduct1(View view) {
        mPrice[0]=1.3;
        mPrice[1]=0.3;
        mPrice[2]=2.3;
        startIntent(getString(R.string.product1_name),getString(R.string.product1_description),mPrice,mOrder);
    }

    public void onProduct2(View view) {
        mPrice[0]=0.7;
        mPrice[1]=1.2;
        mPrice[2]=2.1;
        startIntent(getString(R.string.product2_name),getString(R.string.product2_description),mPrice,mOrder);
    }

    public void onProduct3(View view) {
        mPrice[0]=0.5;
        mPrice[1]=0.9;
        mPrice[2]=1.3;
        startIntent(getString(R.string.product3_name),getString(R.string.product3_description),mPrice,mOrder);
    }

    public void onOrder(View view) {
            Intent intent = new Intent(MainActivity.this, OrderActivity.class);
            intent.putExtra("Order", mOrder);
            startActivityForResult(intent, 1);


    }
    public void startIntent(String nName,String description,double[] price, ArrayList<Product> order){
        Intent intent = new Intent(MainActivity.this, ProductActivity.class);
        intent.putExtra("Name", nName);
        intent.putExtra("Description", description);
        intent.putExtra("Price",price);
        intent.putExtra("Order",order);
        startActivityForResult(intent,1);
    }
    public void Toast(String text)
    {
        Toast toast = Toast.makeText(getApplicationContext(),
                text,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        Bundle bundle = data.getExtras();
        mOrder = (ArrayList<Product>) bundle.getSerializable("Order");
    }
}
