package witaless.simplecart;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class ProductActivity extends MainActivity {
    private TextView mName;
    private TextView mDescription;
    private Button mButton1;
    private Button mButton2;
    private Button mButton3;
    private double[] mPrice = new double[3];
    private ArrayList<Product> mOrder = new ArrayList<>();
    private byte num =-1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        mName = (TextView) findViewById(R.id.textViewName);
        mName.setText(getIntent().getExtras().getString("Name"));
        mDescription = (TextView) findViewById(R.id.textViewDescription);
        mDescription.setText(getIntent().getExtras().getString("Description"));
        mPrice = getIntent().getExtras().getDoubleArray("Price");
        mButton1 = (Button) findViewById(R.id.buttonPrice1);
        mButton1.setText(Double.toString(mPrice[0])+"$");
        mButton2 = (Button) findViewById(R.id.buttonPrice2);
        mButton2.setText(Double.toString(mPrice[1])+"$");
        mButton3 = (Button) findViewById(R.id.buttonPrice3);
        mButton3.setText(Double.toString(mPrice[2])+"$");
        mOrder =(ArrayList<Product>) getIntent().getExtras().get("Order");
    }

    public void onPrice1(View view) {
        mButton1.setBackgroundResource(R.drawable.pressed);
        mButton2.setBackgroundResource(R.drawable.unpressed);
        mButton3.setBackgroundResource(R.drawable.unpressed);
        num = 0;
    }

    public void onPrice2(View view) {
        mButton1.setBackgroundResource(R.drawable.unpressed);
        mButton2.setBackgroundResource(R.drawable.pressed);
        mButton3.setBackgroundResource(R.drawable.unpressed);
        num = 1;
    }

    public void onPrice3(View view) {
        mButton1.setBackgroundResource(R.drawable.unpressed);
        mButton2.setBackgroundResource(R.drawable.unpressed);
        mButton3.setBackgroundResource(R.drawable.pressed);
        num = 2;
    }

    public void onAdd(View view) {
        if(num>-1)
        {
            mOrder.add(new Product(getIntent().getExtras().getString("Name"),mPrice[num]));
            Toast(getIntent().getExtras().getString("Name")+" - "+mPrice[num]+"$ добавлен в корзину!");
        }
        else Toast("Выберите цену!");
    }

    public void onBack(View view) {
        Intent intent = new Intent();

        intent.putExtra("Order", mOrder);
        setResult(1, intent);
        finish();

    }
    public void onBackPressed()
    {
        Intent intent = new Intent();

        intent.putExtra("Order", mOrder);
        setResult(1, intent);
        finish();
    }



}
