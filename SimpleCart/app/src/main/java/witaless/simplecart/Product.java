package witaless.simplecart;

import java.io.Serializable;

public class Product implements Serializable {
    protected String name;
    protected double price;
    Product(String name, double price){

        this.name = name;
        this.price = price;
    }

}
