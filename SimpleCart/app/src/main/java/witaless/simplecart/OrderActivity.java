package witaless.simplecart;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;


public class OrderActivity extends MainActivity {
    private ArrayList<Product> mOrder= new ArrayList<>();
    private String mTextForMessage ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        mOrder =(ArrayList<Product>) getIntent().getExtras().get("Order");
        ListView listOfOrder = (ListView) findViewById(R.id.listOfOrders);
        if(mOrder.size()>0) {
            String[] list = new String[mOrder.size()+1];
            double sumOfPrices =0; //общая стоимость
            for (int i = 0; i < list.length-1; i++) {

                list[i] = mOrder.get(i).name + " - " + mOrder.get(i).price+"$";
                mTextForMessage = mTextForMessage +list[i]+"\n";
                sumOfPrices=sumOfPrices+mOrder.get(i).price;
            }
            sumOfPrices = Math.rint(10.0 * sumOfPrices) / 10.0;
            list[list.length-1]="Сумма= "+sumOfPrices+"$";
            mTextForMessage = mTextForMessage +list[list.length-1];
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                    android.R.layout.simple_list_item_1, list);
            listOfOrder.setAdapter(adapter);
        }
        else {
            String[] list = new String[1];
            list[0]="Корзина пуста";
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                    android.R.layout.simple_list_item_1, list);
            listOfOrder.setAdapter(adapter);
        }

    }

    public void onSend(View view) {
        if (isNetworkAvailable() && mTextForMessage.length()>0 ) {
            Thread thread = new Thread(new Runnable() {

                public void run() {
                    try  {
                        String email = "witaless99@yandex.ru";
                        // Сюда необходимо подставить SMTP сервер, используемый для отправки
                        String host = "smtp.yandex.ru";
                        // Тут указываем порт SMTP сервера.
                        int port = 465;

                        // Создание свойств, получение сессии
                        Properties props = new Properties();

                        // При использовании статического метода Transport.send()
                        // необходимо указать через какой хост будет передано сообщение
                        props.put("mail.smtp.host", host);
                        // Если почтовый сервер использует SSL
                        props.put("mail.smtp.ssl.enable", "true");
                        // Указываем порт SMTP сервера.
                        props.put("mail.smtp.port", port);
                        // Большинство SMTP серверов, используют авторизацию.
                        props.put("mail.smtp.auth", "true");
                        // Включение debug-режима
                        props.put("mail.debug", "true");
                        // Авторизируемся.
                        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
                            // Указываем логин пароль, от почты, с которой будем отправлять сообщение.
                            @Override
                            protected PasswordAuthentication getPasswordAuthentication() {
                                return new PasswordAuthentication("witaless99", "7883567pin");
                            }
                        });

                        try {
                            // Создание объекта сообщения
                            Message msg = new MimeMessage(session);

                            // Установка атрибутов сообщения
                            msg.setFrom(new InternetAddress(email));
                            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
                            msg.setSubject("Новый заказ!");
                            msg.setSentDate(new Date());

                            // Установка тела сообщения
                            msg.setText(mTextForMessage);

                            // Отправка сообщения
                            Transport.send(msg);
                        } catch (MessagingException mex) {
                            // Печать информации об исключении в случае его возникновения
                            mex.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            thread.start();
            Toast("Заказ оформлен.");

        }
        else
        {
            if (mTextForMessage.length()==0)
                Toast("Корзина пуста.");
        }

    }

    public void onClear(View view) {
        mOrder.clear();
        mOrder.trimToSize();
        mTextForMessage ="";
        ListView listOfOrder = (ListView) findViewById(R.id.listOfOrders);
        String[] list = new String[1];
        list[0]="Корзина пуста";
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, list);
        listOfOrder.setAdapter(adapter);

    }

    public void onBackOrder(View view) {
        Intent intent = new Intent();

        intent.putExtra("Order", mOrder);
        setResult(1, intent);
        finish();
    }
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(activeNetworkInfo != null){
            return true;
        }
        else{
            Toast("Нет подключения к сети.");
            return false;
        }
    }
    public void onBackPressed()
    {
        Intent intent = new Intent();

        intent.putExtra("Order", mOrder);
        setResult(1, intent);
        finish();
    }

}
