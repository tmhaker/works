db.metadatas.drop();
codes = db.tasks.distinct('code');
data = codes.map(c => {
    return {code: c, count: db.tasks.count({code: c})};
});
db.metadatas.insert(data);