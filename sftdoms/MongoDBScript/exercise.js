db.exercises.drop();
db.exercises.insert([
    {code: '1.1', title: 'test1', tasks: [{code: '1.1.0', amount: 4}]},
    {code: '2.1', title: 'test2', tasks: [{code: '1.1.0', amount: 2}, {code: '1.2.1', amount: 4}]},
    {code: '2.2', title: 'test3', tasks: [{code: '1.3.1', amount: 4}]},
    {code: '3.1', title: 'test4', tasks: [{code: '1.4.1', amount: 4}]},
    {code: '4.1', title: 'test5', tasks: [{code: '1.3.1', amount: 4}]},
    {code: '4.2', title: 'test6', tasks: [{code: '1.3.2', amount: 2}, {code: '1.4.2', amount: 4}]},
    {code: '5.1', title: 'test6', tasks: [{code: '1.3.2', amount: 2}, {code: '1.2.2', amount: 4}]},
    {code: '5.2', title: 'test6', tasks: [{code: '1.3.3', amount: 4}, {code: '1.2.2', amount: 4}]}
]);