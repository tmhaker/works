db.tasks.insert([
    {
        "code": "1.4.2",
        "sentence": "Ранку ___ и залили йодом..",
        "possibleAnswers": ["отмыли", "обмыли"],
        "ans": "обмыли"
    },
    {
        "code": "1.4.2",
        "sentence": "Собака грозно ___, защищая дом.",
        "possibleAnswers": ["лает", "воет"],
        "ans": "лает"
    },
    {
        "code": "1.4.2",
        "sentence": "Зима ___ землю в белый наряд.",
        "possibleAnswers": ["одела", "надела"],
        "ans": "одела"
    },
    {
        "code": "1.4.2",
        "sentence": "Котёнок жалобно ___.",
        "possibleAnswers": ["мяукал", "мурчал"],
        "ans": "мяукал"
    },
    {
        "code": "1.4.2",
        "sentence": "Девочка ___ новую шубку.",
        "possibleAnswers": ["надела", "одела"],
        "ans": "надела"
    }
]);