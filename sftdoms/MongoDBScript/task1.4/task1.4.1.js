db.tasks.insert([
    {
        "code": "1.4.1",
        "sentence": "Весной ___ снег.",
        "possibleAnswers": ["выпал", "растаял"],
        "ans": "растаял"
    },
    {
        "code": "1.4.1",
        "sentence": "От города к городу ведут широкие ___.",
        "possibleAnswers": ["дороги", "тропинки"],
        "ans": "дороги"
    },
    {
        "code": "1.4.1",
        "sentence": "Бабушка ___ шарф.",
        "possibleAnswers": ["вяжет", "шьет"],
        "ans": "вяжет"
    },
    {
        "code": "1.4.1",
        "sentence": "Зимой на ветвях лежит ___.",
        "possibleAnswers": ["снег", "иней"],
        "ans": "снег"
    },
    {
        "code": "1.4.1",
        "sentence": "Вчера был сильный ___.",
        "possibleAnswers": ["дождь", "гроза"],
        "ans": "дождь"
    }
]);