db.tasks.insert([
    {
        "code": "1.3.2",
        "image": "http://localhost:3000/static/task1.3/ris2.jpg",
        "questions": [
            "Кто?",
            "Что делает?",
            "Каких?",
            "Кого?"
        ]
    },
    {
        "code": "1.3.2",
        "image": "http://localhost:3000/static/task1.3/ris3.png",
        "questions": [
            "Кто?",
            "Что делает?",
            "Какой?",
            "Когда?"
        ]
    },
    {
        "code": "1.3.2",
        "image": "http://localhost:3000/static/task1.3/ris4.png",
        "questions": [
            "Кто?",
            "Что делает?",
            "Под каким?",
            "Под чем?"
        ]
    },
    {
        "code": "1.3.2",
        "image": "http://localhost:3000/static/task1.3/ris5.png",
        "questions": [
            "Кто?",
            "Что делает?",
            "Какую?",
            "Что?"
        ]
    },
    {
        "code": "1.3.2",
        "image": "http://localhost:3000/static/task1.3/ris6.jpg",
        "questions": [
            "Кто?",
            "Что делает?",
            "Что?",
            "Чем?"
        ]
    },
    {
        "code": "1.3.2",
        "image": "http://localhost:3000/static/task1.3/ris7.jpg",
        "questions": [
            "Кто?",
            "Что делает?",
            "Какого?",
            "Кого?"
        ]
    },
    {
        "code": "1.3.2",
        "image": "http://localhost:3000/static/task1.3/ris8.jpg",
        "questions": [
            "Кто?",
            "Что делает?",
            "Какую?",
            "Кого?"
        ]
    },
    {
        "code": "1.3.2",
        "image": "http://localhost:3000/static/task1.3/ris9.jpg",
        "questions": [
            "Кто?",
            "Что делает?",
            "Какими?",
            "Чем?"
        ]
    },
    {
        "code": "1.3.2",
        "image": "http://localhost:3000/static/task1.3/ris10.jpg",
        "questions": [
            "Какой?",
            "Кто?",
            "Что делает?",
            "Как?"
        ]
    },
    {
        "code": "1.3.2",
        "image": "http://localhost:3000/static/task1.3/ris11.jpg",
        "questions": [
            "Кто?",
            "Что делает?",
            "На каком?",
            "На чем?"
        ]
    },
    {
        "code": "1.3.2",
        "image": "http://localhost:3000/static/task1.3/ris12.jpg",
        "questions": [
            "Кто?",
            "Что делают?",
            "Когда?",
            "Чем?"
        ]
    },
    {
        "code": "1.3.2",
        "image": "http://localhost:3000/static/task1.3/ris13.png",
        "questions": [
            "Кто?",
            "Что делают?",
            "Какую?",
            "Что?"
        ]
    },
    {
        "code": "1.3.2",
        "image": "http://localhost:3000/static/task1.3/ris14.jpg",
        "questions": [
            "Кто?",
            "Что делает?",
            "На чем?",
            "Куда?"
        ]
    },
    {
        "code": "1.3.2",
        "image": "http://localhost:3000/static/task1.3/ris15.jpg",
        "questions": [
            "Кто?",
            "Что делают?",
            "На чем?",
            "Как?"
        ]
    },
]);