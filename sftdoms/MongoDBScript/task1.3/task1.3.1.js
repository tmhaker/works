db.tasks.insert([

    {
        "code": "1.3.1",
        "image": "http://localhost:3000/static/task1.3/ris1.png",
        "questions": [
            "Кто?",
            "Что делает?",
            "Откуда?"
        ]
    },
    {
        "code": "1.3.1",
        "image": "http://localhost:3000/static/task1.3/ris2.jpg",
        "questions": [
            "Кто?",
            "Что делает?",
            "Кого?"
        ]
    },
    {
        "code": "1.3.1",
        "image": "http://localhost:3000/static/task1.3/ris3.png",
        "questions": [
            "Кто?",
            "Что делает?",
            "Когда?"
        ]
    },
    {
        "code": "1.3.1",
        "image": "http://localhost:3000/static/task1.3/ris4.png",
        "questions": [
            "Кто?",
            "Что делает?",
            "Под чем?"
        ]
    },
    {
        "code": "1.3.1",
        "image": "http://localhost:3000/static/task1.3/ris5.png",
        "questions": [
            "Кто?",
            "Что делает?",
            "Что?"
        ]
    },
    {
        "code": "1.3.1",
        "image": "http://localhost:3000/static/task1.3/ris6.jpg",
        "questions": [
            "Кто?",
            "Что делает?",
            "Чем?"
        ]
    },
    {
        "code": "1.3.1",
        "image": "http://localhost:3000/static/task1.3/ris7.jpg",
        "questions": [
            "Кто?",
            "Что делает?",
            "Кого?"
        ]
    },
    {
        "code": "1.3.1",
        "image": "http://localhost:3000/static/task1.3/ris8.jpg",
        "questions": [
            "Кто?",
            "Что делает?",
            "Кого?"
        ]
    },
    {
        "code": "1.3.1",
        "image": "http://localhost:3000/static/task1.3/ris9.jpg",
        "questions": [
            "Кто?",
            "Что делает?",
            "Чем?"
        ]
    },
    {
        "code": "1.3.1",
        "image": "http://localhost:3000/static/task1.3/ris10.jpg",
        "questions": [
            "Кто?",
            "Что делает?",
            "Как?"
        ]
    },
    {
        "code": "1.3.1",
        "image": "http://localhost:3000/static/task1.3/ris11.jpg",
        "questions": [
            "Кто?",
            "Что делает?",
            "На чем?"
        ]
    },
    {
        "code": "1.3.1",
        "image": "http://localhost:3000/static/task1.3/ris12.jpg",
        "questions": [
            "Кто?",
            "Что делают?",
            "Чем?"
        ]
    },
    {
        "code": "1.3.1",
        "image": "http://localhost:3000/static/task1.3/ris13.png",
        "questions": [
            "Кто?",
            "Что делают?",
            "Что?"
        ]
    },
    {
        "code": "1.3.1",
        "image": "http://localhost:3000/static/task1.3/ris14.jpg",
        "questions": [
            "Кто?",
            "Что делает?",
            "На чем?"
        ]
    },
    {
        "code": "1.3.1",
        "image": "http://localhost:3000/static/task1.3/ris15.jpg",
        "questions": [
            "Кто?",
            "Что делают?",
            "На чем?"
        ]
    },
]);