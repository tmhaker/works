const storage = {};

const isExist = (token) => {
    return token in storage;
};

const add = (token, data) => {
    //let a = storage.push(token);
    // if (isExist(token)) {
    //     return storage.length;
    // }
    storage[token] = data;
    //return a;
};

const remove = (token) => {
    if (token in storage) {
        delete storage[token];
        return true;
    }
    return false;
};

const addData = (token, data) => {
    storage[token] = data;
};

const get = (token) => {
    return storage[token];
};

module.exports = {
    isExist,
    add,
    remove,
    addData,
    get
};