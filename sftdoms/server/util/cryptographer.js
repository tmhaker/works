const crypto = require('crypto');
const jwtWorker = require('jsonwebtoken');

const md5Encode = (data) => {
    return crypto.createHash('md5').update(data).digest("hex");
};

const generateJwtString = (user, key) => {
    return jwtWorker.sign(user, key);
};

const verifyJwt = (token, key) => {
    try {
        jwtWorker.verify(token, key);
        return true;
    } catch (err) {
        return false;
    }
};

module.exports = {
    md5Encode,
    generateJwtString,
    verifyJwt
};