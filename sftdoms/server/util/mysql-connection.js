const Sequelize = require("sequelize");
const sequelize = new Sequelize("sftdoms", "root", "root", {
    dialect: "mysql",
    host: "localhost",
    define: {
        timestamps: false,
        freezeTableName: true
    }
});

module.exports.connection = sequelize;
//module.exports.define = sequelize.define;