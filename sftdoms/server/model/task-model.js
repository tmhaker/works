const connection = require('../util/mongodb-connection');
const taskSchema = new connection.Schema({
    code: String,
    images: [String],
    possibleAns: [String],
    ans: String
}, {versionKey: false});

const taskModel = connection.model('task', taskSchema);

module.exports = taskModel;

