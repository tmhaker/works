const connection = require('../util/mongodb-connection');
const exerciseSchema = new connection.Schema({
        code: String,
        title: String,
        tasks:
            [{code: String, amount: Number}]
    }
    , {versionKey: false});

module.exports = connection.model('exercise', exerciseSchema);