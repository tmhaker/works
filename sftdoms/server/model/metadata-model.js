const connection = require('../util/mongodb-connection');
const metadataSchema = new connection.Schema({
    code: String,
    count: Number
}, {versionKey: false});

const metadataModel = connection.model('metadata', metadataSchema);

module.exports = metadataModel;

