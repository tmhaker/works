//const User = require('../model/user-model');
const JwtStorage = require('../util/jwt-storage');
const cryptographer = require('../util/cryptographer');
const userService = require('../service/user-service');


const signUp = (req, res) => {
    userService.addNewUser(req.body).then(user => {
        let jwt = cryptographer.generateJwtString({userLogin: user.login}, user.password);
        JwtStorage.add(jwt, null);
        res.header('json-web-token', jwt).sendStatus(201);
        console.log('singup correct: ' + user.dataValues);
    }).catch(err => {
        if (err.ans) {
            console.log('singup incorrect: ' + err.ans.msg);
            res.status(406).json(err.ans);
            return;
        }
        console.log(err);
        res.status(500).json(err);
    });
};

const signIn = (req, res) => {
    userService.getUserByLoginAndPassword(req.body).then(user => {
        let jwt = cryptographer.generateJwtString({userLogin: user.login}, user.password);
        JwtStorage.add(jwt, null);
        res.status(202).json({'json-web-token': jwt});
        console.log('sign in is correct');
    }).catch(err => {
        if (err.ans) {
            console.log('singin incorrect: ' + err.ans.msg);
            if (err.ans.code === 1) {
                res.statusMessage = 'incorrect login';
                res.status(227).json(err.ans);
                return;
            }
            if (err.ans.code === 2) {
                res.statusMessage = 'incorrect password';
                res.status(228).json(err.ans);
                return;
            }
            res.status(500).json(err.ans);
            return
        }
        console.log(err);
        res.status(500).json(err);
    });
};

const signOut = (req, res) => {
    const tokenValue = JwtStorage.remove(req.headers['json-web-token']);
    if (!tokenValue) {
        res.sendStatus(401);
        return;
    }
    res.status(200).json();
};

const isSignedIn = (req, res) => {
    if (JwtStorage.isExist(req.headers['json-web-token'])) {
        res.sendStatus(200);
    } else {
        res.sendStatus(204);
    }
};

module.exports = {
    signUp,
    signIn,
    signOut,
    isSignedIn
};
