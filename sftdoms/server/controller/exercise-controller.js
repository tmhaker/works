const JwtStorage = require('../util/jwt-storage');
const exerciseService = require('../service/exercise-service');
const JWT_HEADER = 'json-web-token';

const createHandler = (req, res) => {
    if (!req.body.code) {
        res.sendStatus(400);
    }

    exerciseService.createExerciseHandler(req.body).then((handler) => {
        const jwt = req.header[JWT_HEADER];
        JwtStorage.addData(jwt, {handler: handler});
        res.status(201).send();
    }).catch(err => {
        res.status(500).json(err);
    });
};

const getTaskFromExercise = (req, res) => {
    const handler = JwtStorage.get(req.header[JWT_HEADER]).handler;
    if (!handler) {
        res.status(204).json({msg: 'you did not choose exercise'});
        return;
    }
    handler.next().then(task => {
        if (!task) {
            res.status(202).json({msg: 'well done. you completed all the tasks'});
            return;
        }
        res.json(task[0]);
    }).catch(err => {
        res.status(500).json(err);
    });
};

const checkCorrectnessOfTask = (req, res) => {
    exerciseService.isCorrectExerciseAnswer(req.body).then(result => {
        res.json(result);
    }).catch(err => {
        res.status(500).json(err);
    });
};

const getAllExercises = (req, res) => {
    exerciseService.getAllExercises().then((exercises) => {
        res.json(exercises);
    }).catch((err) => {
        res.status(500).json(err);
    });
};

module.exports = {
    createHandler,
    getTaskFromExercise,
    checkCorrectnessOfTask,
    getAllExercises
};