const express = require("express");
const userController = require('./controller/user-controller');
const userRouter = require('./router/user-router');
const exerciseRouter = require('./router/exercise-router');
const jsonParser = express.json();
const JwtStorage = require('./util/jwt-storage');
const cors = require('cors');

const app = express();

app.use(jsonParser);
app.use(cors());

app.get("/", function (request, response) {

    response.send("<h1>Главная страница</h1>");
});
app.get("/about", function (request, response) {

    response.send("<h1>О сайте</h1>");
});
app.get("/contact", function (request, response) {

    response.send("<h1>Контакты</h1>");
});

app.post('/user/signup', userController.signUp);
app.post('/user/signin', userController.signIn);
app.use('/static', express.static('public'));

const jwtCheck = (req, res, next) => {
    console.log('json-web-token header check');
    const jwt = req.headers['json-web-token'];
    if (!jwt) {
        res.sendStatus(401);
        return;
    }
    if (!JwtStorage.isExist(jwt)) {
        res.sendStatus(401);
    }
    next();
};

app.use('/user', jwtCheck);
app.use('/exercise', jwtCheck);
//app.use('/static', jwtCheck);
app.use('/user', userRouter);
app.use('/exercise', exerciseRouter);


app.listen(3000);
console.log('Server has been started!!!');