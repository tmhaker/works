const express = require("express");
const userController = require("../controller/user-controller");
const userRouter = express.Router();

// userRouter.post('/signin', userController.signIn);
userRouter.post('/signout', userController.signOut);
userRouter.get('/issignedin', userController.isSignedIn);


module.exports = userRouter;