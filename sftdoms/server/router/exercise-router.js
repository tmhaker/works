const express = require("express");
const exerciseController = require('../controller/exercise-controller');
const exerciseRouter = express.Router();

exerciseRouter.post('/createhandler', exerciseController.createHandler);
exerciseRouter.get('/gettask', exerciseController.getTaskFromExercise);
exerciseRouter.post('/checktask', exerciseController.checkCorrectnessOfTask);
exerciseRouter.get('/getexercises', exerciseController.getAllExercises);

module.exports = exerciseRouter;