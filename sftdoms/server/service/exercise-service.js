const taskModel = require('../model/task-model');
const exerciseModel = require('../model/exercise-model');
const metadataModel = require('../model/metadata-model');

const msg1 = 'exercise with this code does not exist';
const msg2 = 'task id has not been sent';

const createError = (msg, ans) => {
    let e = new Error(msg);
    e['ans'] = ans;
    return e;
};

const createExerciseHandler = (exerciseCodeObj) => {
    console.log(exerciseCodeObj);
    return exerciseModel.findOne(exerciseCodeObj).then(exercise => {
        if (!exercise) throw createError(msg1, {code: 0, msg: msg1});

        return {
            tasks: exercise.tasks,

            next: function () {
                if (this.tasks.length < 1) {
                    return new Promise((resolve, reject) => {
                        resolve(null);
                    })
                }

                const countMap = {};
                const r = Math.floor(Math.random() * this.tasks.length);
                const task = this.tasks[r];
                const count = countMap[task.code];
                task.amount--;
                if (task.amount < 1) {
                    this.tasks.splice(r, 1);
                }

                if (count) {
                    const randomValue = Math.floor(Math.random() * count);
                    return taskModel.find({code: task.code}).select('-ans -possibleAns').limit(1).skip(randomValue);
                } else {
                    return metadataModel.findOne({code: task.code}).then(metadata => {
                        countMap[task.code] = metadata.count;
                        const randomValue = Math.floor(Math.random() * metadata.count);
                        return taskModel.find({code: task.code}).select('-ans -possibleAns').limit(1).skip(randomValue);
                    });
                }
            }
        }
    })
};

const isCorrectExerciseAnswer = (answer) => {
    console.log(answer);
    return taskModel.findById(answer._id).then((task) => {
        if (!answer._id) throw createError('msg2', {msg: msg2});

        if (!task.ans && task.possibleAns.length > 0) return {
            isCorrect: true,
            possibleAns: task.possibleAns
        };

        if (!task.ans) return {
            isCorrect: true
        };

        if (answer.ans === task.ans) return {
            isCorrect: true
        };
        return {
            isCorrect: false
        }
    });
};

const getAllExercises = () => {
    return exerciseModel.find().select('-tasks');
};

module.exports = {
    createExerciseHandler,
    isCorrectExerciseAnswer,
    getAllExercises
};

