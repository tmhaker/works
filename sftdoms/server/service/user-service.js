const userModel = require('../model/user-model');
const cryptographer = require('../util/cryptographer');
const seq = require('sequelize');
const UniqueConstraintError = seq.UniqueConstraintError;
const ValidationError = seq.ValidationError;

const userKeys = ['email', 'login', 'password', 'repPassword'];
const uniq = '_UNIQUE';
const msg1 = 'user-service addNewUser; ';
const msg2 = ' argument is null or undefined';
const msg3 = ' has not been sent';
const msg4 = 'repeated password is incorrect';
const msg5 = ' with the same name already exists';
const msg6 = 'user with this login does not exist';
const msg7 = 'password is incorrect';


const createError = (msg, ans) => {
    let e = new Error(msg);
    e['ans'] = ans;
    return e;
};

const checkUserFields = (user) => {
    for (let i in userKeys) {
        if (!user[userKeys[i]]) throw createError(msg1 + userKeys[i] + msg2, {
            code: i,
            path: userKeys[i],
            msg: userKeys[i] + msg3
        });
    }
};

const addNewUser = (user) => {
    //checkUserFields(user);

    if (!user.password && !user['repPassword'] && user.password !== user['repPassword']) throw createError(msg4, {
        code: -1,
        msg: msg4
    });

    delete user['repPassword'];
    user.role = 'client';
    user.password = cryptographer.md5Encode(user.password);

    return userModel.create(user).catch(err => {
        if (err instanceof UniqueConstraintError) {
            let path = err.errors[0].path.replace(uniq, '');
            let msg = path + msg5;
            throw createError(msg, {code: -2, path: path, msg: msg});
        }
        if (err instanceof ValidationError) {
            let path = err.errors[0].path;
            let msg = path + msg3;
            throw createError(msg, {code: 0, path: path, msg: msg});
        }
        throw err;
    });
};

const getUserByLoginAndPassword = (userDto) => {
    return userModel.findOne({where: {login: userDto.login}, raw: true}).then(user => {
        if (user) {
            const isCorrectPassword = cryptographer.md5Encode(userDto.password) === user.password;
            if (isCorrectPassword) {
                return user;
            } else {
                throw createError(msg7, {code: 2, msg: msg7});
            }
        } else {
            throw createError(msg6, {code: 1, msg: msg6})
        }
    });
};

module.exports = {
    addNewUser,
    getUserByLoginAndPassword
};