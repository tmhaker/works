package privet.ot.muhammeda;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText mEditText;
    Button mButton;
    EditText mEditNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEditText=(EditText) findViewById(R.id.editText);
        mButton=(Button) findViewById(R.id.wapp_button);
        mEditNumber=(EditText) findViewById(R.id.editNumber);

    }

    public void onClickWapp(View view) {
        String txt = mEditText.getText().toString();
        String number=mEditNumber.getText().toString();
        Uri uri = Uri.parse(number);

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, txt);
        sendIntent.setType("text/plain");
        sendIntent.setPackage("com.whatsapp");
        startActivity(sendIntent);



    }

    public void onClickViber(View view) {
        String txt = mEditText.getText().toString();
        String number=mEditNumber.getText().toString();

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(android.content.Intent.EXTRA_TEXT,txt);
        intent.setType("text/plain");
        intent.setPackage("com.viber.voip");
        startActivity(intent);



    }

    public void onClickSkype(View view) {
        String txt = mEditText.getText().toString();
        String number=mEditNumber.getText().toString();

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(android.content.Intent.EXTRA_TEXT,txt);
        intent.setType("text/plain");
        intent.setPackage("com.skype.raider");
        startActivity(intent);

    }

    public void onClickSms(View view) {
        String txt = mEditText.getText().toString();
        String number=mEditNumber.getText().toString();

        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setData(Uri.parse("smsto:"));
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address", number);
        smsIntent.putExtra("sms_body",txt);
        startActivity(smsIntent);
    }

    public void onClickEmail(View view) {
        String txt = mEditText.getText().toString();
        String email=mEditNumber.getText().toString();

        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + email));
        intent.putExtra(Intent.EXTRA_TEXT, txt);

        startActivity(intent);

    }
}
